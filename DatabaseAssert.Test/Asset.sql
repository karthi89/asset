﻿CREATE TABLE [dbo].[Asset](
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[AssetId] VARCHAR(MAX) NOT NULL,
	[FileName] VARCHAR(MAX) NOT NULL,
	[MimeType] VARCHAR(MAX) NOT NULL,
	[CreatedBy] VARCHAR(MAX) NOT NULL,
	[Email] VARCHAR(MAX) NOT NULL,
	[Country] VARCHAR(MAX) NOT NULL,
	[Description] VARCHAR(MAX) NOT NULL
)
