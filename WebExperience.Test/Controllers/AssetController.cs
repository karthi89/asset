﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GeneralKnowledge.Test.App;

namespace WebExperience.Test.Controllers
{
    [RoutePrefix("api/Asset")]
    public class AssetController : ApiController
    {
        // TODO:
        // Create an API controller via REST to perform all CRUD operations on the asset objects created as part of the CSV processing test
        // Visualize the assets in a paged overview showing the title and created on field
        // Clicking an asset should navigate the user to a detail page showing all properties
        // Any data repository is permitted
        // Use a client MVVM framework
        // GET api/values
        private readonly AssertEntities _oAssertEntities = new AssertEntities();

        /// <summary>
        /// Gets this instance.
        /// </summary>
        /// <returns></returns>
        [Route("Get")]
        public IEnumerable<Asset> Get()
        {
            return  _oAssertEntities.Asset.Take(100).ToList();
        }

        /// <summary>
        /// Gets the specified asset identifier.
        /// </summary>
        /// <param name="id">The asset identifier.</param>
        /// <returns></returns>
        [HttpGet]
        public Asset Get(string id)
        {
            var asset = _oAssertEntities.Asset.Find(id);
            return asset;
        }

        /// <summary>
        /// Posts the specified asset.
        /// </summary>
        /// <param name="asset">The asset.</param>
        [HttpPost]
        public void Post(Asset asset)
        {
            _oAssertEntities.Asset.Add(asset);
            _oAssertEntities.SaveChanges();
        }

       [HttpDelete]
        public void Delete(string id)
        {
            var asset = _oAssertEntities.Asset.Find(id);
            _oAssertEntities.Asset.Remove(asset);
            _oAssertEntities.SaveChanges();

        }
       protected override void Dispose(bool disposing)
       {
           if (disposing)
           {
               _oAssertEntities.Dispose();
           }
           base.Dispose(disposing);
       }

    }

 
}
