﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GeneralKnowledge.Test.App;

namespace WebExperience.Test.Controllers
{
    public class HomeController : Controller
    {
        private AssertEntities db = new AssertEntities();
        static HttpClient client = new HttpClient();


        public ActionResult List()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,AssetId,FileName,MimeType,CreatedBy,Email,Country,Description")] Asset asset)
        {
            if (ModelState.IsValid)
            {
              client.PostAsJsonAsync(Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath,"")+"/api/Asset/Create", asset);
               
                return RedirectToAction("List");
            }

            return View(asset);
        }
      
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}