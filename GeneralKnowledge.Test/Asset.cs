//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.ComponentModel.DataAnnotations;

namespace GeneralKnowledge.Test.App
{
    using System;
    using System.Collections.Generic;

    public partial class Asset
    {
        public int Id { get; set; }
        [Required]
        public string AssetId { get; set; }
        [Required]

        public string FileName { get; set; }
        [Required]

        public string MimeType { get; set; }
        [Required]

        public string CreatedBy { get; set; }
        [Required]

        public string Email { get; set; }
        [Required]

        public string Country { get; set; }
        [Required]

        public string Description { get; set; }
    }
}
