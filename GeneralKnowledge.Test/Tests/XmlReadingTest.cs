﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// This test evaluates the 
    /// </summary>
    public class XmlReadingTest : ITest
    {
        public string Name { get { return "XML Reading Test"; } }

        public void Run()
        {
            var xmlData = Resources.SamplePoints;

            // TODO: 
            // Determine for each parameter stored in the variable below, the average value, lowest and highest number.
            // Example output
            // parameter   LOW AVG MAX
            // temperature   x   y   z
            // pH            x   y   z
            // Chloride      x   y   z
            // Phosphate     x   y   z
            // Nitrate       x   y   z

            PrintOverview(xmlData);
        }

        private void PrintOverview(string xml)
        {
            var xmlDoc = XDocument.Parse(xml);
            var xmlValue = (from prod in xmlDoc.Descendants("measurement")
                            let attribute   = prod.Attribute("date")
                            where attribute!= null
                            let date        = attribute.Value
                            let temperature = GetAttributeValue(prod, "temperature")
                            let ph          = GetAttributeValue(prod, "pH")
                            let chloride    = GetAttributeValue(prod, "Chloride")
                            let phosphate   = GetAttributeValue(prod, "Phosphate")
                            let nitrate     = GetAttributeValue(prod, "Nitrate")
                            select new
                            {
                                Date        = Convert.ToDateTime(date),
                                Temperature = temperature.Any() ? Convert.ToDouble(temperature.First().Value) : 0,
                                pH          = GetInteger(ph),
                                Chloride    = GetInteger(chloride),
                                Phosphate   = GetInteger(phosphate),
                                Nitrate     = GetInteger(nitrate),
                            }).ToList();
            var temperatureData             = xmlValue.Where(s => Math.Abs(s.Temperature) > 0).Select(d => d.Temperature).ToList();
            var pHData                      = xmlValue.Where(s => s.pH       != 0).Select(d => d.pH).ToList();
            var chlorideData                = xmlValue.Where(s => s.Chloride != 0).Select(d => d.Chloride).ToList();
            var phosphateData               = xmlValue.Where(s => s.Phosphate!= 0).Select(d => d.Phosphate).ToList();
            var nitrateData                 = xmlValue.Where(s => s.Nitrate  != 0).Select(d => d.Nitrate).ToList();

            Console.WriteLine(@"parameter	LOW	AVG	MAX");
            Console.WriteLine(@"temperature	{0}	{1}	{2}", temperatureData.Min() , temperatureData.Average().ToString("#.##"), temperatureData.Max());
            Console.WriteLine(@"pH		{0}	{1}	{2}"    , pHData.Min()          , pHData.Average().ToString("#.##")         , pHData.Max());
            Console.WriteLine(@"Chloride	{0}	{1}	{2}", chlorideData.Min()    , chlorideData.Average().ToString("#.##")   , chlorideData.Max());
            Console.WriteLine(@"Phosphate	{0}	{1}	{2}", phosphateData.Min()   , phosphateData.Average().ToString("#.##")  , phosphateData.Max());
            Console.WriteLine(@"Nitrate		{0}	{1}	{2}", nitrateData.Min()     , nitrateData.Average().ToString("#.##")    , nitrateData.Max());
        }

        /// <summary>
        /// Gets the integer.
        /// </summary>
        /// <param name="elements">The elements.</param>
        /// <returns></returns>
        private static int GetInteger(IEnumerable<XElement> elements)
        {
            elements = elements.ToList();
            return elements.Any() ? Convert.ToInt32(elements.First().Value) : 0;
        }

        /// <summary>
        /// Gets the attribute value.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="atrribute">The atrribute.</param>
        /// <returns></returns>
        private static IEnumerable<XElement> GetAttributeValue(XContainer container, string atrribute)
        {
            return container.Descendants("param").Where(s =>
            {
                var xAttribute = s.Attribute("name");
                return xAttribute != null && xAttribute.Value == atrribute;
            });
        }
    }
}
