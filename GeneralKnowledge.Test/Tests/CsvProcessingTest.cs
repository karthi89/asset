﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// CSV processing test
    /// </summary>
    public class CsvProcessingTest : ITest
    {
        public void Run()
        {
            // TODO: 
            // Create a domain model via POCO classes to store the data available in the CSV file below
            // Objects to be present in the domain model: Asset, Country and Mime type
            // Process the file in the most robust way possible
            // The use of 3rd party plugins is permitted

            var csvFile = Resources.AssetImport;
            var assetImportList = (from variable in csvFile.Split('\n').Skip(1)
                select variable.Split(',').ToList()
                into a
                where a.Count > 1
                select new AssetImport()
                {
                    AssetId = a[0], FileName = a[1], MimeType = a[2], CreatedBy = a[3], Email = a[4], Country = a[5], Description = a[6]
                }).ToList();

            AssertEntities oAssertEntities = new AssertEntities();

            foreach (var asset in assetImportList)
            {

                oAssertEntities.Asset.Add(new Asset
                {
                    AssetId = asset.AssetId,
                    FileName = asset.FileName,
                    MimeType = asset.MimeType,
                    CreatedBy = asset.CreatedBy,
                    Email = asset.Email,
                    Country = asset.Country,
                    Description = asset.Description
                });
            }
            oAssertEntities.SaveChanges();
        }
    }


    public class AssetImport
    {
        public string AssetId { get; set; }
        public string FileName { get; set; }
        public string MimeType { get; set; }
        public string CreatedBy { get; set; }
        public string Email { get; set; }
        public string Country { get; set; }
        public string Description { get; set; }
    }


}
