﻿namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Image Size
    /// </summary>
    public enum Size { Thumbnail, Preview }
}
